import { Component, Input, OnInit } from '@angular/core';
import { Movie } from './../../interfaces/movie.interface';
import { StorageService } from './../services/storage.service';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.css']
})
export class FilmComponent implements OnInit {

  @Input() film!: Movie;

  constructor(private storageService: StorageService) { }

  ngOnInit(): void {
  }

  openInNewTab(url: string) {
    this.storageService.openTab(url);
  }

  saveFilm(id: string) {
    this.storageService.saveFilm(id);
  }

  deleteFilm(id: string) {
    this.storageService.deleteFilm(id);
  }

}
