import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  openTab(url: string) {
    //@ts-ignore
    return window.open(url, '_blank').focus();
  }

  saveFilm(id: any) {
    const savedFilms = this.getSavedFilms();
    localStorage.setItem('savedFilms', JSON.stringify(savedFilms.concat(id)));
  }

  deleteFilm(id: string) {
    const savedFilms = this.getSavedFilms();
    localStorage.setItem('savedFilms', JSON.stringify(savedFilms.filter((film: string) => film != id )));
  }

  getSavedFilms() {
    //@ts-ignore
    return JSON.parse(localStorage.getItem('savedFilms')) || [];
  }
}
