export interface Movie {
    id: string;
    title: string;
    trailer: string;
    poster: string;
}