import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { TopFilmsRoutingModule } from './top-films-routing.module';
import { TopFilmsComponent } from './top-films/top-films.component';
import { TopFilmsService } from './top-films.service';
import { SharedModule } from './../../shared/shared.module';

@NgModule({
  declarations: [
    TopFilmsComponent
  ],
  imports: [
    CommonModule,
    TopFilmsRoutingModule,
    HttpClientModule,
    SharedModule
  ],
  providers: [
    TopFilmsService
  ],
  bootstrap: []
})
export class TopFilmsModule { }
