import { Component, OnInit } from '@angular/core';
import { TopFilmsService } from './../top-films.service';
import { Movie } from 'src/app/interfaces/movie.interface';


@Component({
  selector: 'app-top-films',
  templateUrl: './top-films.component.html',
  styleUrls: ['./top-films.component.css']
})
export class TopFilmsComponent implements OnInit {

  moviesList: Movie[] = [];

  constructor(private topFilmsService: TopFilmsService) { }

  ngOnInit(): void {
    this.topFilmsService.getFilms().subscribe(({data}: any) => {
      this.moviesList = this.topFilmsService.parseResponse(data);
    })
  }

}
