import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { SavedFilmsComponent } from './saved-films/saved-films.component';
import { SharedModule } from './../../shared/shared.module';
import { SavedFilmsRoutingModule } from './saved-films-routing.module';
import { TopFilmsService } from '../top-films/top-films.service';



@NgModule({
  declarations: [
    SavedFilmsComponent
  ],
  imports: [
  CommonModule,
    SharedModule,
    HttpClientModule,
    SavedFilmsRoutingModule
  ],
  providers: [
    TopFilmsService
  ]
})
export class SavedFilmsModule { }
