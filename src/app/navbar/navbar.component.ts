import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavButtons } from './../interfaces/nav-button.interface';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  navButtons: NavButtons[] = [
    {
      buttonName: 'Home',
      path: '',
    },
    {
      buttonName: 'Saved',
      path: 'saved-films',
    }
  ];

  constructor(private router: Router) { }

  navigate(path: any) {
    this.router.navigate([path]);
  }

  ngOnInit(): void {
  }

}
